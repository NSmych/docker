#!/bin/sh

docker-machine create --driver virtualbox --virtualbox-memory "4096" --virtualbox-cpu-count "2" Char IP=$(docker-machine ip Char) .
docker build -t ex03 .
docker run -it --rm -p 80:80 -p 8022:22 -p 443:443 --privileged ex03
