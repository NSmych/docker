#!/bin/sh

cd ft-rails && docker build . -t ft-rails:on-build && cd ..
docker build . -f Dockerfile -t ft-rails:running
docker run -d -p 3000:3000 --name=kek_rails ft-rails:running
