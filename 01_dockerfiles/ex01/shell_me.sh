#!/bin/sh

echo "Building"
docker build -t ex01 .

echo "Builded, running"
docker run -d --name teamspeak --rm -p 9987:9987/udp -p 10011:10011 -p 30033:30033 ex01
